const { Database } = require('sqlite3').verbose()
const DBSOURCE = "db.sqlite"

const db = new Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message)
        throw err
    } else {
        console.log('Connected to the SQlite database.')
        db.run(`CREATE TABLE user (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name text, 
            email text UNIQUE, 
            password text, 
            CONSTRAINT email_unique UNIQUE (email)
            )`, (err) => {
            if (err) {
                // Table already created
            } else {
                // Table just created, creating some rows
                var insert = 'INSERT INTO user (name, email, password) VALUES (?,?,?)'
                db.run(insert, ["admin", "admin@example.com", '$2b$10$DcQW9f3CS4VJ5v18.FGDLeD21.AxiFr5Clib8tQUVJqmbyQLyt/4y'])
                db.run(insert, ["user", "user@example.com", '$2b$10$DcQW9f3CS4VJ5v18.FGDLeD21.AxiFr5Clib8tQUVJqmbyQLyt/4y'])
            }
        })

        db.run(`CREATE TABLE post (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title text, 
            description text,
            image_url text
            )`, (err) => {
            if (err) {

            } else {
                var insert = 'INSERT INTO post (title, description, image_url) VALUES (?,?,?)'
                    // Table already created
                db.run(insert, ["Lenguajes preferidos", "desc", 'image.jpg'])
                    // Table just created, creating some rows
                    //var insert = 'INSERT INTO user (name, email, password) VALUES (?,?,?)'
                    //db.run(insert, ["admin", "admin@example.com", md5("admin123456")])
                    //db.run(insert, ["user", "user@example.com", md5("user123456")])
            }
        })

        db.run(`CREATE TABLE bienes (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            rut TEXT,
            codigo_clase INTEGER,
            descripcion_clase TEXT,
            codigo_subclase INTEGER,
            descripcion_subclase TEXT,
            num_dt TEXT,
            num_inventario INTEGER,
            num_serie TEXT,
            fecha_asignacion DATE,
            estado_sigas TEXT,
            condicion TEXT,
            centro_costo INTEGER,
            descripcion_centro_costo TEXT
            )`, (err) => {
            if (err) {
                console.log('Tabla Bienes Error', err.message)
            } else {
                var insert = 'INSERT INTO bienes (rut, codigo_clase, descripcion_clase, codigo_subclase, descripcion_subclase, num_dt, num_inventario, num_serie, fecha_asignacion, estado_sigas, condicion, centro_costo, descripcion_centro_costo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)'                
                db.run(insert, ["15867668-0",10,"Escritorio",100,"Tipo L con Cajones","DT-5635",43526345,"23R23S32H6","11-08-2019","Alta","Bueno",8080,"808080808080"])
                db.run(insert, ["15867668-0",11,"Silla",110,"Reclinable","DT-6666",986879,"SERT59809C","10-08-2019","Alta","Bueno",8080,"808080808080"])
                db.run(insert, ["15867668-0",90,"Notebook",900,"Tactil","DT-9999",534566,"SER89696T","11-12-2019","Alta","Bueno",8080,"808080808080"])
                db.run(insert, ["15867668-0",91,"Monitor",910,"20 Pulgadas","DT-9999",123523,"I8578KF","11-12-2019","Alta","Bueno",8080,"808080808080"])
                db.run(insert, ["15867668-0",500,"Soporte Notebook",510,"Soportes Ventilados","DT-5555",235634,"34GWRF21","30-12-2019","Alta","Bueno",8080,"808080808080"])
            }
        })

    }
})

module.exports = db