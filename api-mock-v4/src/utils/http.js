const User = require('../domain/entities/User');
const Bienes = require('../domain/entities/Bienes');

module.exports.getRequestParam = (param, request) => request.params[param];

module.exports.getBodyParam = (param, body, generateError = false) => {
    const value = body[param];
    if (value) {
        return value;
    }
    throw new Error(`[${param}] is required`);
};

module.exports.makeUserByRequestBody = (body) => {
    return new User(
        body.id,
        body.name,
        body.email,
        body.password
    );
};

module.exports.makeBienByRequestBody = (body) => {    
    return new Bienes(
        body.id,
        body.rut,
        body.codigoClase,
        body.descripcionClase,
        body.codigoSubClase,
        body.descripcionSubClase,
        body.numDt,
        body.numInventario,
        body.numSerie,
        body.fechaAsignacion,
        body.estadoSigas,
        body.condicion,        
        body.centroCosto,
        body.descripcionCentroCosto
    );    
};

module.exports.makePostByRequestBody = (body) => {
    return body;
};