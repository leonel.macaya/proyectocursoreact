const BienesPersist = require('../../infrastructure/port/bienesPersist');
const { getRequestParam, makeBienByRequestBody } = require('../../utils/http');
const Encrypt = require('../../infrastructure/port/encrypt');


class BienesController {
    debugger;
    constructor() {
        this.bienesPersist = new BienesPersist();
        this.encrypt = new Encrypt();
    }

    getAll = (req, res, next) => {
        debugger;
        this.debugger;
        const p = this.bienesPersist.getAll();
        p.then(bienes => res.json({ message: 'success', data: bienes }));
        p.catch(err => res.status(400).json({ error: err }));
    }

    get = (req, res, next) => {
        this.debugger;
        const id = getRequestParam('id', req);
        const p = this.bienesPersist.get(id);
        //p.then(bienes => res.json(bienes));
        p.then(bienes => res.json({ message: 'success', data: bienes }));
        p.catch(err => res.status(400).json({ error: err }));
    }

    update = (req, res, next) => {
        this.debugger;
        const updateBien = makeBienByRequestBody(req.body);
        const id = getRequestParam('id', req);        
        updateBien.id = id;
        const pUpdate = this.bienesPersist.update(updateBien);
        pUpdate.then(bienes => res.json({ message: 'success', data: bienes }));
        pUpdate.catch(err => res.status(400).json({ error: err }));
    }

}

module.exports = BienesController;