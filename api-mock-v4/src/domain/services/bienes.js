const Bienes = require('../entities/Bienes');

const createBienesIdOnly = (p = {}) => new Bienes(p.id, null, null, null, null, null, null, null, null, null, null, null, null, null);
const createBienes = (p = {}) => new Bienes(p.id, p.rut, p.codigo_clase, p.descripcion_clase, p.codigo_subclase, p.descripcion_subclase, p.num_dt, p.num_inventario, p.num_serie, p.fecha_asignacion, p.estado_sigas, p.condicion, p.centro_costo, p.descripcion_centro_costo);
const createBienesList = (data = []) => data.map(createBienes);

const createBienesPublic = (p = {}) => new Bienes(p.id, p.rut, p.codigo_clase, p.descripcion_clase, p.codigo_subclase, p.descripcion_subclase, p.num_dt, p.num_inventario, p.num_serie, p.fecha_asignacion, p.estado_sigas, p.condicion, p.centro_costo, p.descripcion_centro_costo);
const createBienesListPublic = (data = []) => data.map(createBienesPublic);

debugger;

module.exports.createBienesIdOnly = createBienesIdOnly;
module.exports.createBienes = createBienes;
module.exports.createBienesList = createBienesList;
module.exports.createBienesPublic = createBienesPublic;
module.exports.createBienesListPublic = createBienesListPublic;