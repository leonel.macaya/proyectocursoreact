class Bienes {

    id = 0;
    rut = '';
    codigo_clase = 0;
    descripcion_clase = '';
    codigo_subclase = 0;
    descripcion_subclase = '';
    num_dt = '';
    num_inventario = 0;
    num_serie = '';
    fecha_asignacion = '01-01-1900';
    estado_sigas = '';
    condicion = '';
    centro_costo = 0;
    descripcion_centro_costo = '';
    
    constructor(id, rut, codigo_clase, descripcion_clase, codigo_subclase, descripcion_subclase, num_dt, num_inventario, num_serie, fecha_asignacion, estado_sigas, condicion, centro_costo, descripcion_centro_costo) {
        this.setId(id);
        this.rut = rut;
        this.codigo_clase = codigo_clase;
        this.descripcion_clase = descripcion_clase;
        this.codigo_subclase = codigo_subclase;
        this.descripcion_subclase = descripcion_subclase;
        this.num_dt = num_dt;
        this.num_inventario = num_inventario;
        this.num_serie = num_serie;
        this.fecha_asignacion = fecha_asignacion;
        this.estado_sigas = estado_sigas;
        this.condicion = condicion;
        this.centro_costo = centro_costo;
        this.descripcion_centro_costo = descripcion_centro_costo;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = Number.parseInt(id);
    }
}

module.exports = Bienes;