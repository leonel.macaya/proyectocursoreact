const BienesPersistSqlite = require('../adapter/bienesPersistSqlite');

class BienesPersist {

    constructor() {
        this.client = new BienesPersistSqlite();
    }

    async getAll() {
        return await this.client.getAll();
    }

    async get(bienes) {
        return await this.client.get(bienes);
    }

    async update(bienes) {
        return await this.client.update(bienes);
    }
    
}

module.exports = BienesPersist;