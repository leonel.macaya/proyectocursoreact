const BienesOutputHTTP = require('../adapter/bienesOutputHTTP');

class BienesOutput {
    constructor(app) {
        this.bienesOutput = new BienesOutputHTTP(app);
    }

    getAll() {
        this.bienesOutput.getAll();
    }

    get() {
        this.bienesOutput.get();
    }

    update() {
        this.bienesOutput.update();
    }

    init() {
        this.getAll();
        this.get();
        this.update();
    }
}

module.exports = BienesOutput;