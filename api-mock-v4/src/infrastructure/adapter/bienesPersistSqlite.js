const db = require('../../../database/config');
const { createBienesListPublic, createBienes, createBienesPublic, createBienesIdOnly } = require('../../domain/services/bienes');

class BienesPersist {

    getAll() {
        debugger;
        const sql = "select * from bienes";
        const params = [];
        debugger;
        return new Promise((resolve, reject) => {
            db.all(sql, params, (err, rows) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(createBienesListPublic(rows));
                }
            });
        });
    }

    get(id) {
        const sql = "select * from bienes where id = ?";
        const params = [id];
        return new Promise((resolve, reject) => {
            db.get(sql, params, (err, row) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(createBienesPublic(row));
                }
            });
        });
    }

    update(bienes) {
        const params = [bienes.rut, bienes.codigo_clase, bienes.descripcion_clase, bienes.codigo_subclase, bienes.descripcion_subclase, bienes.num_dt, bienes.num_inventario, bienes.num_serie, bienes.fecha_asignacion, bienes.estado_sigas, bienes.condicion, bienes.centro_costo, bienes.descripcion_centro_costo, bienes.id];
        console.log(params);
        console.log(bienes.descripcion_clase);
        console.log(bienes.id);
        console.log("Hola Mundo");
        const sql = `UPDATE bienes set
            rut = coalesce(?,rut),
            codigo_clase = coalesce(?,codigo_clase),
            descripcion_clase = coalesce(?,descripcion_clase),
            codigo_subclase = coalesce(?,codigo_subclase),
            descripcion_subclase = coalesce(?,descripcion_subclase),
            num_dt = coalesce(?,num_dt),
            num_inventario = coalesce(?,num_inventario),
            num_serie = coalesce(?,num_serie),
            fecha_asignacion = coalesce(?,fecha_asignacion),
            estado_sigas = coalesce(?,estado_sigas),
            condicion = coalesce(?,condicion),
            centro_costo = coalesce(?,centro_costo),
            descripcion_centro_costo = coalesce(?,descripcion_centro_costo)
        WHERE id = ?`;
        return new Promise((resolve, reject) => {
            db.all(sql, params, (err, rows) => {
                if (err) {
                    reject(err.message);
                } else {
                    resolve(bienes);
                }
            });
        });
    }
}

module.exports = BienesPersist;