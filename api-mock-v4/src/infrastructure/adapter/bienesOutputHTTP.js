const BienesController = require('../../application/use-case/bienesController');
const { ensureToken } = require('../../utils/middleware');

class BienesOutputHTTP {
    debugger;

    constructor(app) {
        this.debugger;
        this.app = app;
        this.bienesController = new BienesController();
    }

    getAll() {
        this.debugger;
        // para proteger agregar ensureToken
        this.app.get('/api/bienes', ensureToken, this.bienesController.getAll);
    }

    get() {
        this.app.get('/api/bienes/:id', this.bienesController.get);
    }

    update() {
        this.app.patch('/api/bienes/:id', ensureToken, this.bienesController.update);
    }

}

module.exports = BienesOutputHTTP;