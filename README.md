Entrega de proyecto.
    
    Aplicativo cuenta con un home, el cual puede iniciar sesión o registrar.
    El inicio de sesón cuanta con una vista privada el cual es una tabla, el detalle de cada linea se puede revisar y modificar en la opcion de detalles.

    Observaciones

        Se requiere uso de api especial ya que fue modificada.
        Se incorpora api dentro del proyecto "api-mock-v4"

        Test trate de incluir pero no puede, no funcionaron.

        No pude separa los iconos de iniciar sesión y de registro, tambien me falto manejar una barra de menus distinta para la vista publica y 
        la privada o en su defecto una sola con los las opciones correspondientes a cada vista.


    Usuario Valido.
        user: leonel.macaya@seidor.com
        pass: 123456

Saludos.

*************************************************************************************************************************************************************
*************************************************************************************************************************************************************
Proyecto
    Vistas publicas
        Home (Post - Card)
        detalle (Post)
        Login 
        Registro
    vistas Privadas
        Home (Post -- Tabla)
        Detalle (Vista o Modal. Post)
        Creacion (Post)
        Actualizacion (Post)
    Componentes
        Funcional (Por lo menos 1) (Manejar Estados y pasar props)
        Clase (Por lo menos 1) (Manejar Estados y pasar props)
    Redux
        Todos los datos deben obtenerse desde el store
        no es necesario pasar los formularios
        modulos
    estructura
        Idealmente manejar la misma estructura que manejamos en clases, de lo contrario justificar porque se cambia. (Components, containers, store, utils)
    UI 
        Reactstrap
        Responsivo (No lo vimos pero se recomienda)
    Cliente HTTP
        la data se obtiene de una api.
    Autenticacion
        desde una api
        sesion (JWT), no se debe perder al hacer reload.
    Rutas
        Utilizar React Router 
    Test    
        Coverage >= 40%
    Context
        Al menos una propiedad pasada por context
    Documentacion
        JSDOC

major.minor.fix
major: cambios significativo, dejar de ser compatible
minor: addiones/ mejoras / cambia implementacion pero poquito
fix: solucion de errores, transparentes a la implementacion

Metodo
/**
 * Multiplicacion
 * descripcion
 * 
 * @author Claudio Rojas <claudio.dcv@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {number} x - primer numero a multiplicar
 * @param {number} y - segundfo numero a multiplicar
 * @param {*} o - bla
 * @param {*} o.x - bla
 * @param {string} o.x.y - bla
 * @returns {string|number} - bla
 */
export const multiplicacion = (x, y, o) => {
    const result = x * y;

    if (x === 100 && y === 0) return 'gano';
    if (Number.isNaN(result)) return 0;
    return result;
};
/*
var o = {
    x: {
        y: 'Hola'
    }
}
*/