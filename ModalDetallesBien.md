import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, Form, Row, Col } from 'reactstrap';
import useInput from '../../hooks/userInput';

import { updateBienesAsyncActionCreator } from '../../store/modules/bienes/bienes.actions';

/**
 * 
 * @param {*} props 
 * @param {string} props.buttonLabel
 * @param {string} props.className
 * @param {*} props.user
 * @param {Function} props.updateAction
 */
const ModalDetallesBien = (props) => {

    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);
    
    const codigoClase = useInput(props.bienes.descripcion_clase);
    
    const codigo_clase = props.bienes.codigo_clase;
    const descripcion_clase = props.bienes.descripcion_clase;
    const codigo_subclase = props.bienes.codigo_subclase;
    const descripcion_subclase = props.bienes.descripcion_subclase;
    const num_dt = props.bienes.num_dt;
    const num_inventario = props.bienes.num_inventario;
    const num_serie = props.bienes.num_serie;
    const fecha_asignacion = props.bienes.fecha_asignacion;
    const estado_sigas = props.bienes.estado_sigas;
    const condicion = props.bienes.condicion;
    const centro_costo = props.bienes.centro_costo;
    const descripcion_centro_costo = props.bienes.descripcion_centro_costo;
    
    const toggle = (history) =>  {
      setModal(!modal);
    };
    
    const updateBien = () => {
        const bienId = props.bienes.id;
        const updateBienes = {
            codigoClase: codigoClase.value          
        }
        props.updateAction(updateBienes, bienId, toggle);
      }

    // const updateBien = (data, id, toggle) => {
    //     dispatch(updateBienesAsyncActionCreator(data, id));
    //     // toggle(props.history);
    // }

    return (
      <div>
        <Button color="warning" onClick={toggle}>{props.buttonLabel}</Button>
        <Modal isOpen={modal} toggle={toggle} className={props.className}>
            <ModalHeader toggle={toggle}> Detalles del Bien </ModalHeader>
            <ModalBody>
                <Form>
                    <Row form>
                        {/* <Col md={3}>
                            <FormGroup>
                                <Label for="idClase">Id Clase</Label>
                                <Input type="text" name="idClase" id="idClase" value={codigo_clase}></Input>
                            </FormGroup>
                        </Col> */}
                        <Col md={9}>
                            <FormGroup>
                                <Label for="clase">Clase</Label>
                                <Input { ...codigoClase } />
                            </FormGroup>
                        </Col>
                    </Row>
                    {/* <Row form>
                        <Col md={3}>
                            <FormGroup>
                                <Label for="idSubClase">Id SubClase</Label>
                                <Input type="text" name="idSubClase" id="idSubClase" value={codigo_subclase}></Input>
                            </FormGroup>
                        </Col>
                        <Col md={9}>
                            <FormGroup>
                                <Label for="subClase">subClase</Label>
                                <Input type="text" name="subClase" id="subClase" value={descripcion_subclase}></Input>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="numDt">Número DT</Label>
                                <Input type="text" name="numDt" id="numDt" value={num_dt}></Input>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="numInventario">Número Inventario</Label>
                                <Input type="text" name="numInventario" id="numInventario" value={num_inventario}></Input>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="numSerie">Número Serie</Label>
                                <Input type="text" name="numSerie" id="numSerie" value={num_serie}></Input>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="fechaAsignacion">Fecha Asignación</Label>
                                <Input type="text" name="fechaAsignacion" id="fechaAsignacion" value={fecha_asignacion}></Input>
                            </FormGroup>
                        </Col>                        
                        <Col md={4}>
                            <FormGroup>
                                <Label for="estadoSigas">Estado SIGAS</Label>
                                <Input type="text" name="estadoSigas" id="estadoSigas" value={estado_sigas}></Input>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="centroCosto">Centro de Costo</Label>
                                <Input type="text" name="centroCosto" id="centroCosto" value={centro_costo}></Input>
                            </FormGroup>
                        </Col>
                    </Row>            */}
                </Form>

            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={toggle}>Cancelar</Button>{' '}
                <Button color="primary" onClick={updateBien}>Cambiar Estado SIGAS</Button>
            </ModalFooter>
        </Modal>
      </div>
    );

}

export default ModalDetallesBien;