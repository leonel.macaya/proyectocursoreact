import React from 'react';
import { Provider } from 'react-redux';
import './App.css';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'; //Aqui se importa el Switch

import store from './store';

import { PrivateRoute } from './components/private-route/PrivateRoute';
import Table from './components/table/Table';
import TableBienes from './containers/table-bienes/TableBienes';
import NavBar from './components/nav-bar/NavBar';
import Footer from './components/footer/Footer';
import Login from './containers/login/Login';
import Home from './containers/home/Home';
import Register from './containers/register/Register';

import SwitchVisual from './components/switch/Switch';

function App() {
   return (
      <Provider store={store}>
       <div className="App">
         <Router>
           <NavBar></NavBar>
           <br></br>
           <Switch>
           <Route path="/" exact component={Home} />
             <Route path="/login" exact component={Login} />
             <Route path="/register" component={Register} />
             <PrivateRoute path="/private/home" component={TableBienes} />

             {/* <PrivateRoute path="/dashboard/users" exact component={UserListView} /> */}
             {/* <PrivateRoute path="/dashboard/users/view/:id" exact component={UserDetailView} /> */}
             {/* <PrivateRoute path="/dashboard/users/create" exact component={UserCreate} /> */}
             {/* <Route path="/login" exact component={LoginView} /> */}
             <Footer />
           
           </Switch>
           <SwitchVisual></SwitchVisual>
           <Footer></Footer>
         </Router>         
       </div>
      </Provider>
      
  );
    
}

export default App;
