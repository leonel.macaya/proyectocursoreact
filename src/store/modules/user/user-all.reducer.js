import {
    USER_GET_ALL_START,
    USER_GET_ALL_SUCCESS,
    USER_GET_ALL_ERROR,
} from './const';

const initialState = {
    data: [],
    loading: false,
    error: null,
    success: null,
    errorMessage: '',
};

const reducer = (prevState = initialState, action) => {
    switch (action.type) {
        case USER_GET_ALL_START:
            return {
                ...prevState,
                loading: true,
            };
        case USER_GET_ALL_SUCCESS:
            return {
                ...prevState,
                loading: false,
                success: true,
                error: false,
                data: action.payload,
            };
        case USER_GET_ALL_ERROR:
            return {
                ...prevState,
                loading: false,
                success: false,
                error: true,
                errorMessage: action.payload,
            };
        default:
            return prevState;
    }
};

export default reducer;