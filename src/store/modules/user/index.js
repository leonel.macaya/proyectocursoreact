import { combineReducers } from 'redux';
import userSaveReducer from './user-save.reducer';
import usersReducer from './user-all.reducer';

const userReducer = combineReducers({    
    userSave: userSaveReducer,
    users: usersReducer,
});

export default userReducer;