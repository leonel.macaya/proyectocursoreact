import {
    AUTH_LOGIN_START,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGOUT,
    AUTH_LOGIN_ERROR,
} from './const';
import parseJwt from '../../../utils/parseJwt';

const ls = window.localStorage;



// const initialState = {
//     data: localStorage.getItem('jwt') ||  null,
//     error: null,
//     success: null,
//     errorMessage: '',
//     loading: false,
// };
const initialState = {
    isLogin: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ls.getItem('token'),
    token: ls.getItem('token') ? ls.getItem('token') : '',
    loading: false,
    errorMessage: '',
    error: null,
    success: null,
    jwt: ls.getItem('token') ? parseJwt(ls.getItem('token')) : {},
};


const loginReducer = (prevState = initialState, action) => {
    switch (action.type) {
        case AUTH_LOGIN_START:
            return {
                ...prevState,
                isLogin: false,
                success: false,
                error: false,
                loading: true,
            };

        case AUTH_LOGIN_SUCCESS:
            ls.setItem('token', action.payload);
            return {
                ...prevState,
                isLogin: true,
                token: action.payload,
                success: true,
                error: false,
                loading: false,
                jwt: parseJwt(action.payload),
            };

        case AUTH_LOGIN_ERROR:
            return {
                isLogin: false,
                token: '',
                success: false,
                error: true,
                errorMessage: action.payload,
                loading: false,
            };

        case AUTH_LOGOUT:
            ls.removeItem('token');
            return {
                ...prevState,
                isLogin: false,
                token: '',
            };
            
        default:
            return prevState;
    }
}

export default loginReducer;