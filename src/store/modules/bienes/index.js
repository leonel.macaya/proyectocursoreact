import { combineReducers } from 'redux';
import bienesReducer from './bienes-all.reducer';

const bienesReducerRoot = combineReducers({        
    bienes: bienesReducer,
});

export default bienesReducerRoot;