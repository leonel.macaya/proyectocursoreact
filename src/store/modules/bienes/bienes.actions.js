import { findAllBienes, findBienById, updateBien } from '../../../services/bienes.services';

import {
    BIENES_GET_ALL_ERROR,
    BIENES_GET_ALL_SUCCESS,
    BIENES_GET_ALL_START,

    BIENES_GET_ERROR,
    BIENES_GET_SUCCESS,
    BIENES_GET_START,

    BIENES_UPDATE_ERROR,
    BIENES_UPDATE_SUCCESS,
    BIENES_UPDATE_START,
} from './const';

//Find All.
const findAllStartActionCreator = () => ({
    type: BIENES_GET_ALL_START,
    payload: null,
});

const findAllOkActionCreator = (data) => ({
    type: BIENES_GET_ALL_SUCCESS,
    payload: data,
});

const findAllNokActionCreator = (errorMessage) => ({
    type: BIENES_GET_ALL_ERROR,
    payload: errorMessage,
});

export const findAllBienesAsyncActionCreator = () => {    
    return (dispatch, getStore) => {        
        dispatch(findAllStartActionCreator());
        findAllBienes()
            .catch(error => {                
                dispatch(findAllNokActionCreator('Error:', error))
            })
            .then(response => {
                if (response.message !== 'success') {                    
                    dispatch(findAllNokActionCreator('Error: generico'))
                } else {                    
                    dispatch(findAllOkActionCreator(response.data))
                }
            });
    }
}

// Bienes by id
const findByIdStartActionCreator = () => ({
    type: BIENES_GET_START,
    payload: null,
});

const findByIdOkActionCreator = (data) => ({
    type: BIENES_GET_SUCCESS,
    payload: data,
});

const findByIdNokActionCreator = (errorMessage) => ({
    type: BIENES_GET_ERROR,
    payload: errorMessage,
});

export const findByIdBienesAsyncActionCreator = (id) => {
    return (dispatch, getStore) => {        
        dispatch(findByIdStartActionCreator());
        findBienById(id)
            .catch(error => {                
                dispatch(findByIdNokActionCreator('Error:', error))
            })
            .then(response => {
                if (response.message !== 'success') {                    
                    dispatch(findByIdNokActionCreator('Error: generico'))
                } else {                    
                    dispatch(findByIdOkActionCreator(response.data))
                }
            });
    }
}

//Update Bien
const startActionCreator = () => ({
    type: BIENES_UPDATE_START,
    payload: null,
})

const successActionCreator = (data) => ({
    type: BIENES_UPDATE_SUCCESS,
    payload: data,
})

const errorActionCreator = (errorMessage) => ({
    type: BIENES_UPDATE_ERROR,
    payload: errorMessage,
})

export const updateBienesAsyncActionCreator = (data, id) => {
    return (dispatch, getStore) => {
        debugger;
        dispatch(startActionCreator());
        const jwt = getStore().bienes.bienes.data;
        updateBien(jwt, data, id).then(data => {
            debugger;
            dispatch(successActionCreator(data.data));
        }).catch(err => {
            debugger;
            dispatch(errorActionCreator(err));
        })
    }
}