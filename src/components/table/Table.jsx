import React, { Component } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Table as TableReactStrap, Button, Container } from 'reactstrap';
import { logoutActionCreator } from '../../store/modules/auth/login.actions';
import Td from '../td/Td';
import Tr from '../tr/Tr';

/**
 * Table
 * Vista de tabla con la carga de bienes.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista tabla.
 * 
 * Esta tabla no se esta actualizando actualmente ya que actualmente se carga otra con los
 * datos de bienes desde api, esta carga desde mock-data
 */
const Table = (props) => {

    const dispatch = useDispatch();

    const handlerLogout = () => {
         dispatch(logoutActionCreator());
    };

    return (
        <div className="container-fluid">
            <br></br>
            <TableReactStrap responsive dark striped bordered hover size="sm">                
                <Tr></Tr>
                <Td></Td>
            </TableReactStrap>
            <Button onClick={handlerLogout}>Cerrar sesión</Button>
        </div>        
    );
};

export default Table;