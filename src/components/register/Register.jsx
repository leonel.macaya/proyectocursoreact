import React, { useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Button, Form, FormGroup, Label, Card, Container, Col, Row, CardHeader, CardBody, CardImg } from 'reactstrap';
import { saveAsyncActionCreator } from '../../store/modules/user/user.actions';

/**
 * Register
 * Objeto de vista Registro.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista registro.
 */
const Register = (props) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();
    const handlerSave = (event) => {
        event.preventDefault();
        const user = {
            name,
            email,
            password,
        };
        dispatch(saveAsyncActionCreator(user));
        window.location = '/';
    }

    const validForm = () => {
        return name === '' || email === '' || password === '';
    };
  
    return (
        
        <Container className="mt-8">
            <br></br>            
            <Row>
                <Col sm={{ size: 5, offset: 3}}>
                    <Card>                        
                        <CardHeader>Formulario de Registro</CardHeader>
                        <CardBody>
                            <Form onSubmit={handlerSave}>
                                <pre className="text-left">
                                </pre>
                                <FormGroup>
                                    <Label>Nombre</Label>
                                    <Input type="text" value={name} onChange={({ target }) => setName(target.value)} placeholder="Ingrese su Nombre." />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Email</Label>
                                    <Input type="email" value={email} onChange={({ target }) => setEmail(target.value)} placeholder="Ingrese su E-Mail." />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Contraseña</Label>
                                    <Input type="password" value={password} onChange={({ target }) => setPassword(target.value)} placeholder="Ingrese su Password." />
                                </FormGroup>                                
                                <br></br>
                                <Button disabled={validForm()} color="primary">Registrar</Button>                                
                            </Form>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};

export default Register;
