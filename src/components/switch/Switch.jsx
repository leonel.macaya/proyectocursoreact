import React, { useContext } from 'react';
import { IoMdMoon as Moon, IoMdSunny as Sun } from 'react-icons/io';

import ThemeContext from '../../context/ThemeContext';

/**
 * Switch
 * Objeto para realizar y usar context
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @returns {*} objeto vista boton..
 */
export default function SwitchVisual () {
  const { dark, toggle } = useContext(ThemeContext);
  
  return (
    <button 
      className='Switch'
      onClick={() => toggle()}
    >
      <Sun className={`icon ${!dark ? 'active' : ''}`}/>
      <Moon className={`icon ${dark ? 'active' : ''}`}/>
    </button>
  );
} 