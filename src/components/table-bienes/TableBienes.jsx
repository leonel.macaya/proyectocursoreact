import React, { useState, useEffect, useContext } from 'react';
import { Container, Button, Table } from 'reactstrap';
import {  useDispatch, useSelector } from 'react-redux';

import { findAllAsyncActionCreator } from '../../store/modules/user/user.actions';
import { findAllBienesAsyncActionCreator, updateBienesAsyncActionCreator, findByIdBienesAsyncActionCreator } from '../../store/modules/bienes/bienes.actions';
import { logoutActionCreator } from '../../store/modules/auth/login.actions';

import ModalDetallesBien from '../../components/modal-detalles-bien/ModalDetallesBien';

/**
 * TablaBienes
 * Objeto vista la cual contiene la carga de los bienes obtenidos desde la api.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista tabla.
 */
const FormTableBienes = (props) => {

    const dispatch = useDispatch();
    const dispatchB = useDispatch();    
    const bienesModule = useSelector(store => store.bienes.bienes);    
    const bienesByIdModule = useSelector(store => store.bienes.bienes);
    
    useEffect(() => {
        dispatch(findAllAsyncActionCreator());
        dispatchB(findAllBienesAsyncActionCreator());
    }, []);

    const handlerLogout = () => {
        dispatch(logoutActionCreator());
    };

    const updateActionBienes = (data, id, toggle) => {
        console.log(data);
        dispatch(updateBienesAsyncActionCreator(data, id));
        toggle(props.history);
        window.location.reload();
    }

    return (
        <Container className="private-home">             
            <br></br>
            <Table dark striped hover responsive>                
                <thead>
                    <tr>
                        <th>#</th>                        
                        <th>Codigo Clase</th>
                        <th>Clase</th>
                        <th>Detalle</th>
                    </tr>
                </thead>                    
                <tbody>
                    {bienesModule.data.map(bienes => (
                        <tr key={bienes.id}>
                            <td>{bienes.id}</td>                            
                            <td>{bienes.codigo_clase}</td>
                            <td>{bienes.descripcion_clase}</td>                            
                            <td>                                
                                <ModalDetallesBien updateActionBienes={updateActionBienes} bienes={bienes} buttonLabel="Detalles" className="" />                                        
                            </td>
                        </tr>
                     ))}
                </tbody>
            </Table>
            <Button onClick={handlerLogout}>Cerrar sesión</Button>
        </Container>   

    );
};

export default FormTableBienes;
