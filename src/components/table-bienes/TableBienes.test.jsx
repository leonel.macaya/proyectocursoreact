import React from 'react';
import ReactDOM from 'react-dom';
import TableBienes from './TableBienes';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TableBienes />, div);
  ReactDOM.unmountComponentAtNode(div);
});
