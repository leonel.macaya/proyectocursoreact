import React, { useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Button, Form, FormGroup, Label, Card, Container, Col, Row, CardHeader, CardBody, CardImg } from 'reactstrap';
//import useInput from '../../hooks/userInput';
import { loginActionsAsyncCreator as loginAction } from '../../store/modules/auth/login.actions';
import ThemeContext from '../../context/ThemeContext';

/**
 * Login
 * Vista Login
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista Login.
 */
const Login = (props) => {

    const { dark } = useContext(ThemeContext);
    const store = useSelector(store => store);
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const buttonIsDisabled = () => password === '' || email === '';

    const handlerSubmit = (event) => {
        event.preventDefault();
        dispatch(loginAction({
            email,
            password,
        }));
    }

    useEffect(() => {        
        if(store.auth.isLogin) {            
            props.history.push('/private/home');
        }
    }, [store.auth.isLogin, props]);

    return (
     
        <Container className="mt-4">            
            <Row>
                <Col sm={{ size: 5, offset: 3}}>
                    <Card>                        
                        <CardHeader>Inicio de sesión</CardHeader>
                        <CardBody>
                            <Form onSubmit={handlerSubmit}>
                                <pre className="text-left">
                                </pre>
                                <FormGroup>
                                    <Label>Email</Label>
                                    <Input type="email" value={email} onChange={({ target }) => setEmail(target.value)} placeholder="Ingrese su E-Mail." />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Contraseña</Label>
                                    <Input type="password" value={password} onChange={({ target }) => setPassword(target.value)} placeholder="Ingrese su Password." />
                                </FormGroup>
                                <br></br>
                                <Button disabled={buttonIsDisabled()} color="primary">Iniciar Sesión</Button>
                                <Button color="danger">{!dark ? 'Light theme' : 'Dark Theme'}</Button>                                
                                {/* <Button disabled={buttonIsDisabled()}>Iniciar Sesión</Button> */}
                                {/* <pre>
                                    {JSON.stringify(store, undefined, 2)}
                                </pre> */}
                            </Form>
                            </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>

    );
};

export default Login;