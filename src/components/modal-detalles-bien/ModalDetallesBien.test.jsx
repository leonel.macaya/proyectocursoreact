import React from 'react';
import ReactDOM from 'react-dom';
import ModalDetallesBien from './ModalDetallesBien';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ModalDetallesBien />, div);
  ReactDOM.unmountComponentAtNode(div);
});
