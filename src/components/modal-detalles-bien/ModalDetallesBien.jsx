import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, Form, Row, Col } from 'reactstrap';
import useInput from '../../hooks/bienesInput';

/**
 * ModalDetallesBien
 * Modal para la carga del detalle de los bienes.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista modal.
 */
const ModalDetallesBien = (props) => {

    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);
    
    const id = useInput(props.bienes.id);
    const rut = useInput(props.bienes.rut);
    const codigoClase = useInput(props.bienes.codigo_clase);
    const descripcionClase = useInput(props.bienes.descripcion_clase);
    const codigoSubClase = useInput(props.bienes.codigo_subclase);
    const descripcionSubClase = useInput(props.bienes.descripcion_subclase);
    const numDt = useInput(props.bienes.num_dt);
    const numInventario = useInput(props.bienes.num_inventario);
    const numSerie = useInput(props.bienes.num_serie);
    const fechaAsignacion = useInput(props.bienes.fecha_asignacion);
    const estadoSigas = useInput(props.bienes.estado_sigas);
    const condicion = useInput(props.bienes.condicion);
    const centroCosto = useInput(props.bienes.centro_costo);
    const descripcionCentroCosto = useInput(props.bienes.descripcion_centro_costo);
    
    const toggle = (history) =>  {
      setModal(!modal);
    };
    
    const updateBien = () => {
        const bienId = props.bienes.id;
        const updateBienes = {
            id: id.value,
            rut: rut.value,
            codigoClase: codigoClase.value,            
            descripcionClase: descripcionClase.value,
            codigoSubClase: codigoSubClase.value,
            descripcionSubClase: descripcionSubClase.value,
            numDt: numDt.value,
            numInventario: numInventario.value,
            numSerie: numSerie.value,
            fechaAsignacion: fechaAsignacion.value,
            estadoSigas: estadoSigas.value,
            condicion: condicion.value,
            centroCosto: centroCosto.value,
            descripcionCentroCosto: descripcionCentroCosto.value
        }
        console.log(updateBienes);
        props.updateActionBienes(updateBienes, bienId, toggle);        
    }

    return (
      <div>
        <Button color="warning" onClick={toggle}>{props.buttonLabel}</Button>
        <Modal isOpen={modal} toggle={toggle} className={props.className}>
            <ModalHeader toggle={toggle}> Detalles del Bien </ModalHeader>
            <ModalBody>
                <Form>
                    <Row form>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="id">Id</Label>
                                <Input disabled={true} { ...id } />
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="rut">Rut</Label>
                                <Input disabled={true} { ...rut } />
                            </FormGroup>
                        </Col>                        
                    </Row>
                    <Row form>
                        <Col md={3}>
                        <FormGroup>
                                <Label for="idClase">Clase</Label>
                                <Input disabled={true} { ...codigoClase } />
                            </FormGroup>
                        </Col>
                        <Col md={9}>
                            <FormGroup>
                                <Label for="descripcionClase">Clase</Label>
                                <Input disabled={true} { ...descripcionClase } />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={3}>
                            <FormGroup>
                                <Label for="codigoSubClase">Id Sub Clase</Label>
                                <Input disabled={true} { ...codigoSubClase } />
                            </FormGroup>
                        </Col>
                        <Col md={9}>
                            <FormGroup>
                                <Label for="descripcionSubClase">Sub Clase</Label>
                                <Input disabled={true} { ...descripcionSubClase } />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="numDt">Número DT</Label>
                                <Input disabled={true} { ...numDt } />
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="numInventario">Número Inventario</Label>
                                <Input disabled={true} { ...numInventario } />
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="numSerie">Número Serie</Label>
                                <Input disabled={true} { ...numSerie } />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="fechaAsignacion">Fecha Asignación</Label>
                                <Input disabled={true} { ...fechaAsignacion } />
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="condicion">Condición</Label>
                                <Input { ...condicion } />
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="estadoSigas">Estado SIGAS</Label>
                                <Input { ...estadoSigas } />
                            </FormGroup>
                        </Col>                        
                    </Row>
                    <Row form>
                        <Col md={4}>
                            <FormGroup>
                                <Label for="centroCosto">Centro de Costo</Label>
                                <Input { ...centroCosto } />
                            </FormGroup>
                        </Col>
                        <Col md={8}>
                            <FormGroup>
                                <Label for="descripcionCentroCosto">Descripción Centro de Costo</Label>
                                <Input { ...descripcionCentroCosto } />
                            </FormGroup>
                        </Col>
                    </Row>
                </Form>

            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={toggle}>Cancelar</Button>{' '}
                <Button color="primary" onClick={updateBien}>Cambiar Estado SIGAS</Button>
            </ModalFooter>
        </Modal>
      </div>
    );

}

export default ModalDetallesBien;