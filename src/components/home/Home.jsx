import React from 'react';
import './Home.css';

/**
 * Home
 * Home de la pagina
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista home.
 */
const Home = (props) => {

    return (
        <div>                
            <header>            
                <div className="hero-image">
                    <div className="hero-text">
                    </div>
                </div>
            </header>
            <body>
            </body>
        </div>
    );
};

export default Home;