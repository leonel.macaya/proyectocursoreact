import React from 'react';
import { Button } from 'reactstrap';
import { bienes } from '../../utils/mock-data';

/**
 * Td
 * Se utiliza para la carga de las filas de la tabla de bienes. mock-data
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista filas de tablas.
 */
const Td = (props) => {

    return (
        <tbody>
            {bienes.map(elm => (
                <tr key={elm.Id}>
                    <td>{elm.Id}</td>
                    <td>{elm.Rut}</td>
                    <td hidden="true">{elm.CodigoClase}</td>
                    <td>{elm.DescripcionClase}</td>
                    <td hidden="true">{elm.CodigoSubClase}</td>
                    <td>{elm.DescripcionSubClase}</td>
                    <td>{elm.NumDT}</td>
                    <td>{elm.NumInventario}</td>
                    <td>{elm.NumSerie}</td>
                    <th>{elm.FechaAsignacion}</th>
                    <th>{elm.EstadoSigas}</th>
                    <th hidden="true">{elm.Condicion}</th>
                    <th>{elm.CentroCosto}</th>
                    <th>{elm.DescripcionCentroCosto}</th>
                    <td>
                        <Button block>Dar de Baja</Button>
                    </td>
                </tr>
            ))}
        </tbody>
    );
};

export default Td;