import React from 'react';
import ReactDOM from 'react-dom';
import Td from './Td';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Td />, div);
  ReactDOM.unmountComponentAtNode(div);
});
