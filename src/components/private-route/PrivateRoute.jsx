import React from 'react';
import { useSelector } from 'react-redux'; 
import { Redirect, Route } from 'react-router-dom';

/**
 * PrivateRoute
 * Objeto de rutas para vistas privadas.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0 
 * @returns {*} objeto.
 */
export const PrivateRoute = ({ component: Component, ...rest}) => {
    const isLogin = useSelector(store => store.auth.isLogin);
    return (
        <Route
            {...rest}
            render={props => 
                isLogin ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/',
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );
};