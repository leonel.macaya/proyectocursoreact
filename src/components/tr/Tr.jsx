import React from 'react';

/**
 * Tr
 * Vista de encabezado de tabla de bienes.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista fila encabezado de tabla..
 */
const Tr = (props) => {

    return (
        <thead>
            <tr>
                <th>Id</th>
                <th>Rut</th>
                <th hidden="true">CodigoClase</th>
                <th>DescripcionClase</th>
                <th hidden="true">CodigoSubClase</th>
                <th>DescripcionSubClase</th>
                <th>NumDT</th>
                <th>NumInventario</th>
                <th>NumSerie</th>
                <th>FechaAsignacion</th>
                <th>EstadoSigas</th>
                <th hidden="true">Condicion</th>
                <th>CentroCosto</th>
                <th>DescripcionCentroCosto</th>
                <th>Accion</th>
            </tr>
        </thead>        
    );
};

export default Tr;