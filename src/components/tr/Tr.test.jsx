import React from 'react';
import ReactDOM from 'react-dom';
import Tr from './Tr';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Tr />, div);
  ReactDOM.unmountComponentAtNode(div);
});
