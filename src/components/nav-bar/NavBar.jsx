import React, {useState} from 'react';
import {
    Collapse,
    Navbar as NavbarRS,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    ToolTip    
    } from 'reactstrap';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { FaUserPlus as AddUser, FaUserLock as IniciarSesion } from 'react-icons/fa';
import Register from '../../containers/register/Register';

/**
 * NavBar
 * Barra de Menus
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} Objeto barra de menus.
 */
const NavBar = (props) => {

    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <NavbarRS color="light" light expand="lg">
                <NavbarBrand href="/"><strong>Aplicación React - Proyecto Final</strong></NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <Link to="/login"><IniciarSesion className="iconUser" tooltip="Iniciar Sesión"></IniciarSesion>         </Link>
                        </NavItem>                        
                        <NavItem> 
                            <Link to="/register"><AddUser className="iconUser" tooltip="Registrarse"></AddUser></Link>
                        </NavItem>                                          
                    </Nav>
                </Collapse>
            </NavbarRS>
        </div>
    );
}

export default NavBar;