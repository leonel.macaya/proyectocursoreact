import React from 'react';
import './Footer.css';

/**
 * Footer
 * Pie de la pagina
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0 
 * @returns {*} objeto
 */
const Footer = () => {
    return (
         <footer className="footer page-footer font-small blue pt-4">
            <div className="container-fluid text-center text-md-left">
                <div className="row">
                    <div className="col-md-6 mt-md-0 mt-3">
                        <p><h5 className="text-uppercase">Leonel Macaya Navarro</h5></p>
                        <p>Curso React - Desafio LATAM</p>
                    </div>
                    <hr className="clearfix w-100 d-md-none pb-3" />
                    <div className="col-md-3 mb-md-0 mb-3">
                    <p><h6 className="text-uppercase">Links</h6></p>
                        <ul className="list-unstyled">
                            <li>
                                <a href="https://es.reactjs.org/">React</a>
                            </li>                            
                        </ul>
                    </div>
                    <div className="col-md-3 mb-md-0 mb-3">
                    <p><h6 className="text-uppercase">Links</h6></p>
                        <ul className="list-unstyled">
                            <li>
                                <a href="https://es.redux.js.org/">Redux</a>
                            </li>                            
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

    );
};

export default Footer;