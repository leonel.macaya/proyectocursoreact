// import axios from 'axios';
// import { HOST } from './config';

// const MODULE = 'login';

// export const loginService = (data) => {
//     debugger;

//     return new Promise((resolve, reject) => {
//         axios.post(`${HOST}/${MODULE}`, data).then(data => {
//             resolve(data.data);
//         }).catch(err => reject(err));
//     })
// }

import { HOST } from './config';

export const loginService = (data) => {    
    return fetch(`${HOST}/login`, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())        
}