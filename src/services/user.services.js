import axios from 'axios';
import { API_HOST } from './config';

const MODULE = 'user';

// export const saveService = (jwt = '', data) => {
//     return new Promise((resolve, reject) => {
//         axios.post(`${API_HOST}/${MODULE}`, data, {
//             headers: {
//                 authorization: `bearer ${jwt}`,
//             }
//         }).then(data => {
//             resolve(data.data);
//         }).catch(err => reject(err.message));
//     })
// }

export const saveService = (user) => {
    const token = localStorage.getItem('token');
    debugger;
    return fetch(`${API_HOST}/user`, {       
        method: 'POST', // or 'PUT'
        body: JSON.stringify(user),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }).then(res => res.json())
};

// export const getAllService = (jwt = '') => {
//     return new Promise((resolve, reject) => {
//         axios.get(`${API_HOST}/${MODULE}`, {
//             headers: {
//                 authorization: `bearer ${jwt}`,
//             }
//         }).then(data => {
//             resolve(data.data);
//         }).catch(err => reject(err.message));
//     })
// }

export const findAll = () => {
    const token = localStorage.getItem('token');
    return fetch(`${API_HOST}/user`, {
        method: 'GET', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }).then(res => res.json())
}

// export const getService = (jwt = '', id) => {
//     return new Promise((resolve, reject) => {
//         axios.get(`${API_HOST}/${MODULE}/${id}`, {
//             headers: {
//                 authorization: `bearer ${jwt}`,
//             }
//         }).then(data => {
//             resolve(data.data);
//         }).catch(err => reject(err.message));
//     })
// }

// export const updateService = (jwt = '', data, id) => {
//     return new Promise((resolve, reject) => {
//         axios.patch(`${API_HOST}/${MODULE}/${id}`, data, {
//             headers: {
//                 authorization: `bearer ${jwt}`,
//             }
//         }).then(data => {
//             resolve(data.data);
//         }).catch(err => reject(err.message));
//     })
// }

// export const deleteService = (jwt = '', id) => {
//     return new Promise((resolve, reject) => {
//         const url = `${API_HOST}/${MODULE}/${id}`;
//         axios.delete(url, { headers: { authorization: `bearer ${jwt}` } })
//             .then(data => {
//                 resolve(data.data);
//             }).catch(err => reject(err.message));
//     })
// }