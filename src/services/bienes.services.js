import axios from 'axios';
import { API_HOST } from './config';

const MODULE = 'bienes';

export const findAllBienes = () => {
    const token = localStorage.getItem('token');    
    return fetch(`${API_HOST}/bienes`, {        
        method: 'GET', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }).then(res => res.json())
}

export const findBienById = (id) => {    
    const token = localStorage.getItem('token');
    return fetch(`${API_HOST}/bienes/${id}`, {
        method: 'GET', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }).then(res => res.json())
};

export const updateBien = (jwt = '', data, id) => {
    return new Promise((resolve, reject) => {
        debugger;
        const jwt = localStorage.getItem('token');
        axios.patch(`${API_HOST}/bienes/${id}`, data, {
            headers: {
                authorization: `bearer ${jwt}`,
            }
        }).then(data => {
            debugger;
            resolve(data.data);
        }).catch(err => reject(err.message));
    })
}

// export function updateBien(id, data) {
//     const token = localStorage.getItem('token');
//     return fetch(`${API_HOST}/bienes/${id}`, {
//         method: 'PUT',
//         mode: 'CORS',        
//         headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${token}`
//         }
//     }).then(data => {
//         return data;
//     }).catch(err => err);
// }

