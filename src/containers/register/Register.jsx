import React from 'react';
import RegisterForm from '../../components/register/Register';

/**
 * Register
 * Objeto de vista Registro.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista registro.
 */
const Register = () => {
   return (
       <div className="register">
           <RegisterForm {...this} />
       </div>
   )
};

export default Register;
