import React from 'react';
import HomeForm from '../../components/home/Home';

/**
 * Home
 * Home de la pagina
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista home.
 */
const Home = (props) => {

    return (
        <div className="home">
           <HomeForm {...this} />
       </div>
    );
};

export default Home;