import React from 'react';
import FormTableBienes from '../../components/table-bienes/TableBienes';

/**
 * TablaBienes
 * Objeto vista la cual contiene la carga de los bienes obtenidos desde la api.
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista tabla.
 */
const TableBienes = () => {
   return (
       <div>
           <FormTableBienes {...this} />
       </div>
   )
};

export default TableBienes;