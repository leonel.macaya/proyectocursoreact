import React, { Component } from 'react';
import LoginForm from '../../components/login/Login';

/**
 * Login
 * Vista Login
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} props
 * @returns {*} objeto vista Login.
 */
class Login extends Component {
     render() {
         return (
             <div className="login">
                 <LoginForm {...this.props} />
             </div>
         )
     }
 };

export default Login;
