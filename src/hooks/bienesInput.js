import { useState, useEffect } from 'react';


/**
 * useInput
 * Hooks
 * 
 * @author Leonel Macaya <leonel.macaya@gmail.com>
 * @since 0.1.0
 * @version 1.0.0
 * @param {*} initialValue
 * @returns {*} objeto
 */
const useInput = (initialValue = '', type = 'text') => {
    const [value, setValue] = useState(initialValue);

    useEffect(() => {
        setValue(initialValue)
    }, [initialValue]);

    return {
        onChange: (event) => setValue(event.target.value),
        value,
        type,
    }
}

export default useInput;