import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

import App from './App';
import * as serviceWorker from './serviceWorker';

import { ThemeProvider } from './context/ThemeContext';

const Root = () => (
    <AlertProvider template={AlertTemplate} >
      <App />
    </AlertProvider>
  )

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<ThemeProvider><Root /></ThemeProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

